# -*- coding: utf-8 -*-
import Queue
import threading

from .announce import announce
from .discover import discover
__author__ = 'ewan'


def start_announcing(api_port, broadcast_address,
                     service_discovery_port, secret_code, run_event):
    announcing_thread = threading.Thread(
            target=announce,
            args=(api_port, broadcast_address,
                  service_discovery_port, secret_code, run_event)
    )
    announcing_thread.start()


def start_discovering(service_discovery_port, secret_code, run_event):
    discovery_thread = threading.Thread(
        target=discover,
        args=(service_discovery_port, secret_code, run_event)
    )
    discovery_thread.start()
