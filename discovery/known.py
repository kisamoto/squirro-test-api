# -*- coding: utf-8 -*-
__author__ = 'ewan'

# My api
my_api_set = set()

# Set of known active API addresses
known_apis_set = set()

known_services_set = set()


def add_to_set(a_set, some_data):
    for address in some_data.split(","):
        if address.strip() != "":
            a_set.add(address)
