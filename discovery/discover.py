# -*- coding: utf-8 -*-
import logging
from socket import socket, AF_INET, SOCK_DGRAM
import time

from .known import (known_apis_set, known_services_set, add_to_set)

__author__ = 'ewan'


def discover(port, secret_code, run_event, delay=1):
    logger = logging.getLogger('squirro_api')
    s = socket(AF_INET, SOCK_DGRAM)  # create UDP socket
    s.bind(('', port))
    while run_event.is_set():
        data, service_address = s.recvfrom(1024)  # wait for a packet
        if data.startswith(secret_code):
            data = data[len(secret_code):]
            if data.startswith('api'):
                logger.debug("got api addresses containing {}".format(
                    data[len('api'):]
                ))
                add_to_set(known_apis_set, data[len('api'):])
            elif data.startswith('service'):
                logger.debug("got service addresses containing {}".format(
                    data[len('service'):]
                ))
                add_to_set(known_services_set, data[len('service'):])
        time.sleep(delay)
