# -*- coding: utf-8 -*-
__author__ = 'ewan'


def string_to_addr(address_string):
    addr = address_string.split(":")
    return (addr[0], int(addr[1]))
