# -*- coding: utf-8 -*-
import logging
import threading
from .service_discovery import (start_announcing, start_discovering)
__author__ = 'ewan'


def start_service_discovery(api_port, broadcast_address,
                            service_discovery_port, secret_code):
    logger = logging.getLogger('squirro_api')
    try:
        api_port = int(api_port)
        service_discovery_port = int(service_discovery_port)
    except ValueError:
        logger.error(
                """Unable to set correct ports for:
\t- api_port = {}
\t- service_discovery_port = {}
Not starting service discovery""".format(
                        api_port, service_discovery_port)
        )
        return
    run_event = threading.Event()
    try:
        run_event.set()
        start_announcing(
                api_port, broadcast_address,
                service_discovery_port, secret_code, run_event
        )
        start_discovering(service_discovery_port, secret_code, run_event)
    except KeyboardInterrupt:
        logger.warning("Received Keyboard Interrupt")
        run_event.clear()
