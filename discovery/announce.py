# -*- coding: utf-8 -*-
import logging
from socket import (socket, AF_INET, SOCK_DGRAM, SOL_SOCKET,
                    SO_BROADCAST, gethostbyname, gethostname)
import time
from .known import (known_apis_set, known_services_set, my_api_set)
from .util import string_to_addr
__author__ = 'ewan'


def announce(api_port, broadcast_address, port,
             secret_code, run_event, delay=5):
    logger = logging.getLogger('squirro_api')
    s = socket(AF_INET, SOCK_DGRAM)  # create UDP socket
    s.bind(('', 0))
    s.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)  # this is a broadcast socket
    # get our IP. Be careful if you have multiple network interfaces or IPs
    my_ip = gethostbyname(gethostname())
    logger.debug("Our IP is: {}".format(my_ip))
    known_apis_set.add("{}:{}".format(my_ip, api_port))
    # This is a horrible hack...
    my_api_set.add("{}:{}".format(my_ip, api_port))
    known_services_set.add("{}:{}".format(my_ip, port))
    while run_event.is_set():
        # Broadcast the secret code, our IP and the port to find the API on.
        # Send all known addresses to share the knowledge
        api_data = "{}api{}".format(secret_code, ','.join(known_apis_set))
        service_data = "{}service{}".format(
                secret_code, ','.join(known_services_set))
        # Send to broadcast address
        s.sendto(api_data, string_to_addr(broadcast_address))
        s.sendto(service_data, string_to_addr(broadcast_address))
        logger.debug("sent service announcement")
        # and now send to all known services
        for service in known_services_set:
            addr = string_to_addr(service)
            s.sendto(api_data, addr)
            s.sendto(service_data, addr)
        time.sleep(delay)
