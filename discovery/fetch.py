# -*- coding: utf-8 -*-
import json
import logging
import urllib2

from document.text import new_text_document_from_json
from storage.cache import memoize
from .known import known_apis_set, my_api_set

__author__ = 'ewan'


@memoize(100)
def remote_fetch(key):
    """Ideally this would use the service discovery tier to locate
    and retrieve documents from remote services (cached in memory for future)

    Args:
         key (string): required document key to retrieve.
    Returns:
        document (Document): If found will return a document, else `None`.
    """
    other_apis = known_apis_set - my_api_set
    logger = logging.getLogger('squirro_api')
    logger.debug("Other API's: {}".format(other_apis))
    for service_address in other_apis:
        service = RemoteService(service_address)
        document = service.retrieve_text_document(key)
        if document is not None:
            return document
    return None


class RemoteService(object):

    _text_document_path = "/v1/document/text/"
    _logger = logging.getLogger('squirro_api')

    def __init__(self, remote_service_address):
        self.remote_service_address = remote_service_address

    def retrieve_text_document(self, key):
        url = "http://{}{}{}.json?internal=true".format(
            self.remote_service_address,
            self._text_document_path,
            key
        )
        self._logger.info("Sending request to {}".format(url))
        req = urllib2.Request(
            url=url,
            headers={"Content-Type": "application/json",
                     "Accept": "application/json"}
        )
        try:
            res = urllib2.urlopen(req)
        except urllib2.URLError, ex:
            self._logger.error("Unable to open URL '{}', got '{}'".format(
                url,
                ex
            ))
            return None
        if res.getcode() is not 200:
            self._logger.error("{} status code returned".format(res.getcode()))
            return None
        res_str = res.read()
        return new_text_document_from_json(json.loads(res_str))
