# -*- coding: utf-8 -*-
from storage import DictStorage
from document.text import TextDocument
from discovery.fetch import remote_fetch

__author__ = 'ewan'


class API(object):

    _middlewares = []
    _storage = DictStorage()

    def __init__(self,
                 middlewares=(),
                 storage=None,
                 ):
        self._middlewares.extend(middlewares)
        if storage is not None:
            self._storage = storage

    def add_middleware(self, middleware):
        self._middlewares.append(middleware)

    def get_document(self, key, fetch_remote=True):
        contents = self._storage.get(key)
        if contents is not None:
            document = TextDocument(doc_id=key, doc_value=contents)
        elif fetch_remote:
            # Try to remote fetch using service discovery. If it's disabled
            # then this will just return None anyway.
            document = remote_fetch(key)
        else:
            document = None
        return document

    def set_document(self, document):
        # Persist the document first
        self._storage.set(document)
        # Then run each middleware on document
        for m in self._middlewares:
            m.process(document)

    def shutdown(self):
        self._storage.flush_close()
