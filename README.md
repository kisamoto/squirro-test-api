# Squirro programming challenge

_author: Ewan Jones_

## Aim

A simple, extendable API capable of persisting and retrieving documents. Ideally horizontally scalable.

## Design

### Ideal

Under _ideal_ conditions, a scalable infrastructure for this project would involve a scalable web tier behind a load balancer, talking to a database (clustered NoSQL or SQL with read replicas) with a caching layer to ease load on the databse.

### Limitations

Due to the limitations of being unable to use anything other than the python standard library, the above scenario is not possible. Instead a local storage system backed by the [`dbm`](https://docs.python.org/2/library/dbm.html) library was chosen as it fundamentally acts as a persistent `dict`.

Caching is still possible, keeping up to 100 elements in memory using a decorator but a little more thought is required to horizontally scale the serivce.

With no code changes a load balancer could be placed in front of 3 instances, with requests routed based on the key sent. This way a GET/SET will always end up on the same server, simply sharding the data. However this has some major disadvantages as the number of instances is basically fixed.

Simple service discovery is required to be able to just add in new instances. With inspiration taken from [this StackOverflow answer](http://stackoverflow.com/a/21090815/1401034) UDP packets are broadcast looking for a "master" and known IP addresses shared. This does not address what happens if a server dies, high availability etc. 

## Running it

### Requirements

In a new `virtualenv` install the limited requirements:

`pip install -r requirements.txt`

### Run - Single instance

`python runserver.py`

Test it's running: 

`curl -X POST -H "Content-Type: application/json" -H "Accept: application/json" -d '{"key": "F51DE427-6694-490C-A90B-055B156052EC", "contents": "Continental does not have an office in Zürich."}' http://localhost:8001/v1/document/text/`

`curl -X GET -H "Content-Type: application/json" -H "Accept: application/json" http://localhost:8001/v1/document/text/F51DE427-6694-490C-A90B-055B156052EC`

### Run - Multiple instances

Each instance needs a unique API port to listen on which can be set as an environment variable. 

Make sure `ENABLE_SERVICE_DISCOVERY = True` in `[local_]settings.py` 

Let's start two services (which need to be killed to stop - see warning below):

```
SQUIRRO_PORT=8001 SQUIRRO_SERVICE_PORT=9001 python runserver.py
SQUIRRO_PORT=8002 SQUIRRO_SERVICE_PORT=9002 python runserver.py
```

We will set a document using the `8001` API instance, and retrieve it from the `8002` instance.

_Create_

`curl -X POST -H "Content-Type: application/json" -H "Accept: application/json" -d '{"key": "F51DE427-6694-490C-A90B-055B156052EC", "contents": "Continental does not have an office in Zürich."}' http://localhost:8001/v1/document/text/`

_Retrieve_

`curl -X GET -H "Content-Type: application/json" -H "Accept: application/json" http://localhost:8002/v1/document/text/F51DE427-6694-490C-A90B-055B156052EC`

## Structure

### Modules

By breaking the API out into a few small modules, code reuse can be increased and these can even be broken out into packages if required. 

#### Document

Barebones module containing a simple `AbstractDocument` and `TextDocument` representation to be used throughout the API. 

#### Middleware

Middleware are run upon insertion of a document and inherit from a `BaseMiddleware` object which times the logic for now. 

Each Middleware object must overwrite the `logic` method that takes a document and does something with it; `logic()` is not expected to return anything.

#### Storage

The `AbstractStorage` class defines `get`, `set` and `flush_close` methods that can be overwritten by any storage engine logic. Currently both `DictStorage` and `DBMStorage` are used but these could easily be expanded to use an in memory DB (Redis), SQL or a raw file system. 

#### Discovery

*Incomplete*

Unfortunately I did not manage to successfully implement service discovery completely. 

*WARNING - at the moment service discovery uses threading and does not cleanly shutdown on a `KeyboardInterrupt`* 

## Deployment

Production deployment can be done using OS level packages (`.deb`, `.rpm` etc) and a custom, private repository.
 
Using version control branches and a build server (e.g. Jenkins) when a code deployment is required the whole process can be automated. 

* Merge changes to be deployed into `production` branch and push;
* Jenkins pulls the latest code, builds the virtualenv from the `requirements.txt`, packages into an OS package and pushes to the repository;
* Configuration management software (Puppet, Chef, Ansible, Salt etc) trigger a server update to pull the latest OS package and install.

If a roll back is required either explicitly set the required OS package version in the configuration manager or roll back in version control, merge, push and let the deployment process continue as normal.

## Time

In total this took around 4.5 hours.
