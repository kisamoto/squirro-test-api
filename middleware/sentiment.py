# -*- coding: utf-8 -*-
from .base import BaseMiddleware

__author__ = 'ewan'


class SentimentAnalysis(BaseMiddleware):

    _name = "sentiment analysis"

    def name(self):
        return self._name

    def logic(self, document):
        self.logger.debug("This is an informative message")
