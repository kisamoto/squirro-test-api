# -*- coding: utf-8 -*-
import logging

from .base import BaseMiddleware

__author__ = 'ewan'


class LanguageDetection(BaseMiddleware):

    _name = "language detection"

    def name(self):
        return self._name

    def logic(self, document):
        logging.debug("Pretty sure this is English. But possibly German?")
