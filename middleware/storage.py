# -*- coding: utf-8 -*-
from .base import BaseMiddleware
__author__ = 'ewan'


class StorageWrapper(BaseMiddleware):
    """
    Simple wrapper to turn a storage engine into middleware.
    Could be used as:
        a) default starting middleware
        b) to update a document after a previous middleware has completed
    """

    _name = u"storage"

    def __init__(self, storage_engine):
        self.storage_engine = storage_engine

    def name(self):
        return self._name

    def logic(self, document):
        self.storage_engine.set(document)
