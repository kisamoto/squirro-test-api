# -*- coding: utf-8 -*-
import logging
import timeit

__author__ = 'ewan'


class AbstractMiddleware(object):

    logger = logging.getLogger('squirro_api')

    def name(self):
        raise NotImplementedError

    def logic(self, document):
        raise NotImplementedError


class BaseMiddleware(AbstractMiddleware):

    def process(self, document):
        self.logger.debug(u"{} - processing document".format(self.name()))
        start = timeit.default_timer()
        self.logic(document)
        stop = timeit.default_timer()
        self.logger.debug(u"{} - document processed in {}s".format(
                self.name(),
                stop-start,
        ))
