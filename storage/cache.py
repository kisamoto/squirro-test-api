# -*- coding: utf-8 -*-
import cPickle

__author__ = 'ewan'


def memoize(function, limit=None):
    """Simple caching decorator taken from
    http://code.activestate.com/recipes/496879/

    Args:
        function (python function or method): Function to be decorated
        limit (Optional[int]): Max number of items to keep in memory

    Returns:
        function: Cached function
    """
    if isinstance(function, int):
        def memoize_wrapper(f):
            return memoize(f, function)

        return memoize_wrapper

    memo_dict = {}
    memo_list = []

    def memoize_wrapper(*args, **kwargs):
        try:
            key = cPickle.dumps((args, kwargs))
        except cPickle.UnpickleableError:
            return function(*args, **kwargs)
        try:
            memo_list.append(memo_list.pop(memo_list.index(key)))
        except ValueError:
            output = function(*args, **kwargs)
            if not output:
                return output
            memo_dict[key] = output
            memo_list.append(key)
            if limit is not None and len(memo_list) > limit:
                del memo_dict[memo_list.pop(0)]

        return memo_dict[key]

    memoize_wrapper._memoize_dict = memo_dict
    memoize_wrapper._memoize_list = memo_list
    memoize_wrapper._memoize_limit = limit
    memoize_wrapper._memoize_origfunc = function
    memoize_wrapper.func_name = function.func_name
    return memoize_wrapper
