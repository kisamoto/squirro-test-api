# -*- coding: utf-8 -*-
import dbm
from .base import AbstractStorage
from .cache import memoize

__author__ = 'ewan'


class DBMStorage(AbstractStorage):

    def __init__(self, db_path):
        self.dbm_db = dbm.open(db_path, 'c')

    def set(self, document):
        # Convert to utf-8 because dbm wants encoded strings
        self.dbm_db[document.id().encode('utf-8')] = \
            document.contents().encode('utf-8')

    @memoize(100)
    def get(self, key):
        doc = self.dbm_db.get(key.encode('utf-8'), None)
        if doc is not None:
            doc = doc.decode('utf-8')
        return doc

    def flush_close(self):
        self.dbm_db.close()
