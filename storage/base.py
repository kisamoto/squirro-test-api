# -*- coding: utf-8 -*-
import logging
__author__ = 'ewan'


class AbstractStorage(object):

    logger = logging.getLogger('squirro_api')

    def get(self, key):
        raise NotImplementedError

    def set(self, document):
        raise NotImplementedError

    def flush_close(self):
        raise NotImplementedError
