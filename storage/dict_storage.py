# -*- coding: utf-8 -*-
from .base import AbstractStorage

__author__ = 'ewan'


class DictStorage(AbstractStorage):

    _dict_store = {}

    def set(self, document):
        self._dict_store[document.id()] = document.contents()

    def flush_close(self):
        """No need to implement flush_close for dict storage as it's purely
        in memory.
        """
        pass

    def get(self, key):
        return self._dict_store.get(key, None)
