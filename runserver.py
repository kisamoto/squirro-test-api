#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os

import wsgiservice as ws

from discovery import start_service_discovery
import log
from routes import *
import settings

__author__ = 'ewan'

app = ws.get_app(globals())

if __name__ == '__main__':
    logger = log.setup_logging('squirro_api')
    api_port = os.environ.get('SQUIRRO_PORT', settings.API_PORT)
    try:
        iport = int(api_port)
    except ValueError:
        logger.warning("Unable to set {} as port, default to 8001".format(
                api_port))
        iport = 8001
    # Optionally enable service discovery in settings.
    # Required for multi instance
    if settings.ENABLE_SERVICE_DISCOVERY:
        service_discovery_port = os.environ.get(
                'SQUIRRO_SERVICE_PORT', settings.SERVICE_DISCOVERY_PORT
        )
        start_service_discovery(
                iport,
                settings.BROADCAST_ADDRESS,
                service_discovery_port,
                settings.SECRET_KEY
        )

    logger.info("Running on port {}".format(iport))
    from wsgiref.simple_server import make_server
    make_server('', iport, app).serve_forever()
