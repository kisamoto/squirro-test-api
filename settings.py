# -*- coding: utf-8 -*-
__author__ = 'ewan'

SECRET_KEY = "insecure_squirro_key"

API_PORT = 8001

SERVICE_DISCOVERY_PORT = 9001

ENABLE_SERVICE_DISCOVERY = False

# Known network IP of a running service
BROADCAST_ADDRESS = 'localhost:9001'

try:
    from local_settings import *
except ImportError:
    pass
