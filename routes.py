# -*- coding: utf-8 -*-
import logging
import os

from wsgiservice import (Resource, mount, raise_404)

from api import API
from document.text import new_text_document_from_json
from middleware.language import LanguageDetection
from middleware.sentiment import SentimentAnalysis
import settings
from storage.dbm_storage import DBMStorage

__author__ = 'ewan'

__all__ = ['TextDocument', 'TextDocuments']

api = API(
    middlewares=[SentimentAnalysis(), LanguageDetection()],
    storage=DBMStorage("api_{}".format(os.environ.get('SQUIRRO_PORT',
                                                      settings.API_PORT)))
)

logger = logging.getLogger('squirro_api')


@mount("/v1/document/text/{document_key}")
class TextDocument(Resource):

    def GET(self, document_key):
        # Only fetch remote if this is NOT an "internal" call
        if self.data.get('internal') is not None:
            doc = api.get_document(document_key, fetch_remote=False)
        else:
            doc = api.get_document(document_key, fetch_remote=True)
        if doc is None:
            raise_404(self)
        return self.to_application_json(doc.to_json())


@mount("/v1/document/text/")
class TextDocuments(Resource):

    def POST(self):
        doc = new_text_document_from_json(self.data)
        api.set_document(doc)
        return self.to_application_json({"success": True})
