# -*- coding: utf-8 -*-
import json
from .base import AbstractDocument

__author__ = 'ewan'


class TextDocument(AbstractDocument):

    def __init__(self, doc_id, doc_value):
        self.doc_id = doc_id
        self.doc_value = doc_value

    def id(self):
        return self.doc_id

    def contents(self):
        return self.doc_value

    def to_json(self):
        return {"key": self.doc_id, "contents": self.doc_value}


def new_text_document_from_json(json_data):
    if isinstance(json_data, unicode):
        json_data = json.loads(json_data)
    return TextDocument(
        doc_id=json_data.get("key"),
        doc_value=json_data.get("contents")
    )
