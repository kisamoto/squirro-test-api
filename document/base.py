# -*- coding: utf-8 -*-
import logging
__author__ = 'ewan'


class AbstractDocument(object):

    logger = logging.getLogger('squirro_api')

    def id(self):
        raise NotImplementedError

    def contents(self):
        raise NotImplementedError

    def to_json(self):
        raise NotImplementedError
